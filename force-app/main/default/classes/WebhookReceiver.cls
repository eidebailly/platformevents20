@RestResource(urlMapping='/webhook')
global with sharing class WebhookReceiver {

    @HttpPost
    global static void receivePost(String recordId, String recordName){
        Webhook__e e = new Webhook__e();
        e.Id__c = recordId;
        e.Name__c = recordName;
        EventBus.publish(e);
    }
}
