trigger AccountCDC on AccountChangeEvent (after insert) {
    for(AccountChangeEvent evt:Trigger.new){

        EventBus.ChangeEventHeader header = evt.ChangeEventHeader;

        for(String recordId:header.recordids){
            Task t = new Task();
            t.Subject = 'Verify ' + header.getChangeType() + ' by ' + header.commituser;
            t.Description = JSON.serialize(header.changedFields);
            t.WhatId = recordId;
            insert t;
        }

    }
}