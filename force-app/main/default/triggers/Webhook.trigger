trigger Webhook on Webhook__e (after insert) {
    List<Account > objs = new List<Account >();

    for(Webhook__e e:Trigger.new){
        Account o = new Account();
        o.Id = e.Id__c;
        o.Name = e.Name__c;
        objs.add(o);
    }

    upsert objs;
}