trigger EventLog on EventLog__e (after insert) {

    for(EventLog__e e:Trigger.new){

        if(Limits.getDmlStatements() == Limits.getLimitDmlStatements()){
            break;
        }

        Log__c l = new Log__c();
        l.User__c = e.Running_User_Id__c;
        l.Message__c = e.Message__c;
        insert l;

        EventBus.TriggerContext.currentContext().setResumeCheckpoint(e.replayId);
    }

}